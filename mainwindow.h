#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QImage>
#include <QDebug>

#include "menuwidget.h"
#include "blurmenuwidget.h"
#include "helpwindow.h"
#include "image.h"
#include "grayscale.h"
#include "average.h"
#include "median.h"
#include "histogram.h"

namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class
 * This class represents main window of the program.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief MainWindow -> default constructor
     * @param parent
     */
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    /**
     * @brief m_menu -> pointer to Menu object
     */
    MenuWidget *m_menu = nullptr;
    /**
     * @brief m_blurMenu -> pointer to BlurMenu object
     */
    BlurMenuWidget *m_blurMenu = nullptr;
    /**
     * @brief m_help -> pointer to Help object
     */
    HelpWindow *m_help = nullptr;
    /**
     * @brief m_image -> pointer to Image object
     */
    Image *m_image = nullptr;

    /**
     * @brief changeStackedWidget -> method that changes menu from
     * Menu to BlurMenu
     */
    void changeStackedWidget();
    /**
     * @brief setMainMenu -> method that changes BlurMenu to Menu
     */
    void setMainMenu();
    /**
     * @brief fillImageStruct -> method that initializes Image object
     * based on loaded image
     * @param inputImage -> Image loaded to program
     */
    void fillImageStruct(const QImage &inputImage);
    /**
     * @brief loadFile -> method that allows to load file
     */
    void loadFile();
    /**
     * @brief saveFile -> method that allows to save file
     */
    void saveFile();
    /**
     * @brief createImageForDisplay -> method that creates QImage for
     * display
     * @param imageStructure -> transformed or not Image object
     */
    void createImageForDisplay(const Image &imageStructure);
    /**
     * @brief displayPhoto -> method that loads photo into label
     * @param m_image -> image object
     */
    void displayPhoto(const QImage &image) const;
    /**
     * @brief showAbout -> method that shows help widget
     */
    void showAbout();
    /**
     * @brief grayscaleTransformation -> method that creates Grayscale
     * object and calls it's transform method
     */
    void grayscaleTransformation();
    /**
     * @brief createHistogram -> method that creates Histogram object
     * and calls it's generateHistogram method
     */
    void createHistogram();
    /**
     * @brief averageTransformation -> method that creates Average object
     * and calls it's transform method
     * @param blurRadius -> radius of average blur
     */
    void averageTransformation(int blurRadius);
    /**
     * @brief medianTransformation -> method that creates Median object
     * and calls it's transform method
     * @param blurRadius -> radius of median blur
     */
    void medianTransformation(int blurRadius);
};

#endif // MAINWINDOW_H
