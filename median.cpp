#include "median.h"

Median::Median(uint8_t givenBlurRadius) :
    Blur(givenBlurRadius)
{

}

Median::~Median()
{

}

uint8_t Median::modifyData(std::vector<uint8_t> mask)
{
    std::nth_element(mask.begin(), mask.begin() + mask.size() / 2, mask.end());
    return mask[mask.size() / 2];
}
