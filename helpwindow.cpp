#include "helpwindow.h"
#include "ui_helpwindow.h"

HelpWindow::HelpWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HelpWindow)
{
    ui->setupUi(this);

    m_shadow = new QGraphicsDropShadowEffect;

    connect(ui->pb_ok, &QPushButton::clicked, this, &HelpWindow::close);

    /* STYLE */
    this->setStyleSheet("background-color: #000d1a;"
                        "color: white;");

    ui->pb_ok->setStyleSheet("background-color: #0080ff;"
                             "font: 14px Courier;"
                             "color: white;"
                             "border-radius: 10px;"
                             "min-width: 8em;"
                             "min-height: 1.5em;");

    m_shadow = new QGraphicsDropShadowEffect;
    m_shadow->setBlurRadius(30);
    m_shadow->setOffset(3, 3);
    m_shadow->setColor(QColor(166, 166, 166, 255));

    this->setGraphicsEffect(m_shadow);
}

HelpWindow::~HelpWindow()
{
    delete ui;
    delete m_shadow;
}
