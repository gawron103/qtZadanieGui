#include "average.h"

Average::Average(uint8_t givenBlurRadius) :
    Blur(givenBlurRadius)
{

}

Average::~Average()
{

}

uint8_t Average::modifyData(std::vector<uint8_t> mask)
{
    return std::accumulate(mask.begin(), mask.end(), 0) / mask.size();
}
