#include "grayscale.h"

Grayscale::Grayscale()
{

}

Grayscale::~Grayscale()
{

}

Image Grayscale::transform(const Image &inputImage)
{
    Image grayscaleImage(inputImage.size, ImageType::Grayscale);

    for(unsigned int i = 0; i < inputImage.data.size(); i += 4)
    {
        grayscaleImage.data.push_back((inputImage.data[i] + inputImage.data[i + 1] + inputImage.data[i + 2]) / 3);
    }

    return grayscaleImage;
}
