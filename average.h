#ifndef AVERAGE_H
#define AVERAGE_H

#include <algorithm>
#include <iostream>
#include <numeric>

#include "blur.h"

/**
 * @brief The Average class
 * The task of this class is to blur image by use of average
 */
class Average : public Blur
{
public:
    /**
     * @brief Average -> constructor
     * @param givenBlurRadius -> radius for blur
     */
    Average(uint8_t givenBlurRadius);
    ~Average();

    /**
     * @brief processOnMask -> public method that return's the average of vector
     * @param mask -> vector of specific pixel and closest neighbors
     * @return average of vector
     */
    uint8_t modifyData(std::vector<uint8_t> mask);
};

#endif // AVERAGE_H
