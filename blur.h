#ifndef BLUR_H
#define BLUR_H

#include <vector>

#include "transformation.h"
#include "grayscale.h"

#include "../googletest/googletest/include/gtest/gtest_prod.h"

/**
 * @brief The Blur class
 * An abstract class that implements its parent method and has its own pure virtual method
 */
class Blur : public Transformation
{
public:
    /**
     * @brief Blur -> constructor
     * @param radius -> radius for blur
     */
    Blur(int radius);
    ~Blur();

    /**
     * @brief transform -> method inherited from class Transformation
     * @param inputImage -> image
     * @return
     */
    Image transform(const Image &inputImage);
    /**
     * @brief processOnMask -> pure virtual function
     * @param mask -> vector of specific pixel and closest neighbors
     * @return struct of modified image
     */
    virtual uint8_t modifyData(std::vector<uint8_t> mask) = 0;

private:
    /**
     * @brief m_BLUR_MASK_RADIUS -> radius for blur
     */
    const uint8_t m_BLUR_MASK_RADIUS;

    FRIEND_TEST(BlurTest, TransformTest);
};

#endif // BLUR_H
