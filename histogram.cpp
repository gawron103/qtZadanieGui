#include "histogram.h"

Histogram::Histogram() :
    m_programStatus(-1)
{

}

Histogram::~Histogram()
{

}

int Histogram::generateHistogram(const Image &inputImage)
{
    if (inputImage.type == ImageType::Rgba)
    {
        const std::vector<std::string> fileNames = {
            "histogramBlue.jpg",
            "histogramGreen.jpg",
            "histogramRed.jpg"
        };

        for (unsigned int i = 0; i < fileNames.size(); i++)
        {
            std::vector<int> dataRgba = createRGBHistogram(inputImage, i);

            m_programStatus =  drawHistogram(dataRgba, fileNames[i]);
        }
    }
    else if (inputImage.type == ImageType::Grayscale)
    {
        const std::string GRAY_HISTOGRAM_FILENAME = "histogramGrayscale.jpg";
        std::vector<int> dataGrayscale = createGrayscaleHistogram(inputImage);

        m_programStatus  = drawHistogram(dataGrayscale, GRAY_HISTOGRAM_FILENAME);
    }

    return m_programStatus;
}

std::vector<int> Histogram::createRGBHistogram(const Image &inputImage, const uint8_t pixelComponent)
{
    std::vector<int> histogramData(255, 0);

    for (unsigned int i = 0; i < inputImage.data.size(); i += 4)
    {
        histogramData[inputImage.data[i + pixelComponent]]++;
    }

    return histogramData;
}

std::vector<int> Histogram::createGrayscaleHistogram(const Image &inputImage)
{
    std::vector<int> histogramData(255, 0);

    for (unsigned int i = 0; i < inputImage.data.size(); i++)
    {
        histogramData[inputImage.data[i]]++;
    }

    return histogramData;
}

int Histogram::drawHistogram(const std::vector<int> &histogramData, const std::string fileName)
{
    const int IMAGE_WIDTH = histogramData.size();
    const int IMAGE_HEIGHT = 255;
    const int DEFAULT_COLOR = 255;
    const int COLUMN_COLOR = 0;

    Image outputImage(Size(IMAGE_WIDTH, IMAGE_HEIGHT), ImageType::Grayscale);
    outputImage.data.resize(outputImage.pixelCount());
    outputImage.fill(DEFAULT_COLOR);

    const int histogram_max = *std::max_element(histogramData.begin(), histogramData.end());

    for (int col = 0; col < IMAGE_WIDTH; col++)
    {
        const int column_height = static_cast<float>(histogramData[col]) / histogram_max * IMAGE_HEIGHT;
        for (int row = 0; row < column_height; row++)
        {
            outputImage.setPixel(IMAGE_HEIGHT - row - 1, col, COLUMN_COLOR);
        }
    }

    m_programStatus = saveHistogramImage(fileName, outputImage.data, IMAGE_WIDTH, IMAGE_HEIGHT);

    return m_programStatus;
}

int Histogram::saveHistogramImage(const std::string &fileName, const std::vector<uint8_t> data, const int width, const int height)
{
    QImage newImage (data.data(), width, height, width, QImage::Format_Grayscale8);

    const bool isSaveSuccess = newImage.save(QString::fromStdString(fileName), "JPG", 100);

    if (!isSaveSuccess)
    {
        std::cout << "Histogram error" << std::endl;
        m_programStatus = -1;

        return m_programStatus;
    }

    std::cout << "Histogram job done!" << std::endl;
    m_programStatus = 0;
    return m_programStatus;
}
