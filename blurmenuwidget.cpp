#include "blurmenuwidget.h"
#include "ui_blurmenuwidget.h"

BlurMenuWidget::BlurMenuWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BlurMenuWidget)
{
    ui->setupUi(this);

    connect(ui->pb_back, &QPushButton::clicked, this, &BlurMenuWidget::backToPreviousMenu);
    connect(ui->pb_average, &QPushButton::clicked, this, &BlurMenuWidget::averageTransformation);
    connect(ui->pb_median, &QPushButton::clicked, this, &BlurMenuWidget::medianTransformation);

    ui->pb_average->setShortcut(QKeySequence("Ctrl+a"));
    ui->pb_median->setShortcut(QKeySequence("Ctrl+m"));
    ui->pb_back->setShortcut(QKeySequence("Ctrl+z"));

    /* STYLE */
    this->setStyleSheet("QPushButton"
                        "{"
                        "background-color: #0080ff;"
                        "color: white;"
                        "font: 14px Courier;"
                        "border-radius: 10px;"
                        "min-width: 10em;"
                        "min-height: 1.75em;"
                        "}");

    m_shadow = new QGraphicsDropShadowEffect;
    m_shadow->setBlurRadius(30);
    m_shadow->setOffset(3, 3);
    m_shadow->setColor(QColor(166, 166, 166, 255));

    this->setGraphicsEffect(m_shadow);
}

BlurMenuWidget::~BlurMenuWidget()
{
    delete ui;
    delete m_shadow;
}

void BlurMenuWidget::backToPreviousMenu()
{
    emit signalBack();
}

void BlurMenuWidget::averageTransformation()
{
    m_blurRadius = ui->horizontalSlider->value();
    emit signalAverageTransformation(m_blurRadius);
}

void BlurMenuWidget::medianTransformation()
{
    m_blurRadius = ui->horizontalSlider->value();
    emit signalMedianTransformation(m_blurRadius);
}
