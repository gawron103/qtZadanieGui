#ifndef BLURMENUWIDGET_H
#define BLURMENUWIDGET_H

#include <QWidget>
#include <QGraphicsDropShadowEffect>

namespace Ui {
class BlurMenuWidget;
}

/**
 * @brief The BlurMenuWidget class
 * The tash of this class is create and show sub-menu with blur
 * options
 */
class BlurMenuWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief BlurMenuWidget -> Constructor
     * @param parent
     */
    explicit BlurMenuWidget(QWidget *parent = 0);
    ~BlurMenuWidget();

private:
    Ui::BlurMenuWidget *ui;

    /**
     * @brief m_shadow -> pointer to QGraphicsDropShadow object
     */
    QGraphicsDropShadowEffect *m_shadow = nullptr;

    /**
     * @brief m_blurRadius -> variable of blur radius
     */
    int m_blurRadius;

    /**
     * @brief backToPreviousMenu -> method that allows to switch between
     * menus
     */
    void backToPreviousMenu();
    /**
     * @brief averageTransformation -> method that takes blur radius from
     * user and emits signal to MainWindow class
     */
    void averageTransformation();
    /**
     * @brief medianTransformation -> method that takes blur radius from
     * user and emits signal to MainWindow class
     */
    void medianTransformation();

signals:
    /**
     * @brief signalBack -> signal for switching between menus
     */
    void signalBack();
    /**
     * @brief signalAverageTransformation -> signal for calling
     * blur by average
     * @param radius -> radius of average blur
     */
    void signalAverageTransformation(int radius);
    /**
     * @brief signalMedianTransformation -> signal for calling
     * blur by median
     * @param radius -> radius of median blur
     */
    void signalMedianTransformation(int radius);
};

#endif // BLURMENUWIDGET_H
