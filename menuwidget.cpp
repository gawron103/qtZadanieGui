#include "menuwidget.h"
#include "ui_menuwidget.h"

MenuWidget::MenuWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuWidget)
{
    ui->setupUi(this);

    connect(ui->pb_blur, &QPushButton::clicked, this, &MenuWidget::clickedBlurButton);
    connect(ui->pb_close, &QPushButton::clicked, this, &MenuWidget::closeApp);
    connect(ui->pb_gray, &QPushButton::clicked, this, &MenuWidget::grayscaleTransformation);
    connect(ui->pb_histogram, &QPushButton::clicked, this, &MenuWidget::createHistogram);

    ui->pb_gray->setShortcut(QKeySequence("Ctrl+g"));
    ui->pb_histogram->setShortcut(QKeySequence("Ctrl+h"));
    ui->pb_blur->setShortcut(QKeySequence("Ctrl+b"));

    /* STYLE */
    this->setStyleSheet("QPushButton {"
                        "background-color: #0080ff;"
                        "color: white;"
                        "font: 14px Courier;"
                        "border-radius: 10px;"
                        "min-width: 10em;"
                        "min-height: 1.75em;}");

    m_shadow = new QGraphicsDropShadowEffect;
    m_shadow->setBlurRadius(30);
    m_shadow->setOffset(3, 3);
    m_shadow->setColor(QColor(166, 166, 166, 255));

    this->setGraphicsEffect(m_shadow);
}

MenuWidget::~MenuWidget()
{
    delete ui;
    delete m_shadow;
}

void MenuWidget::clickedBlurButton()
{
    emit signalShowBlurMenu();
}

void MenuWidget::grayscaleTransformation()
{
    emit signalGrayscaleTransformation();
}

void MenuWidget::createHistogram()
{
    emit signalCreateHistogram();
}

void MenuWidget::closeApp()
{
    emit signalCloseApp();
}
