#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_image = new Image();

    m_menu = new MenuWidget();

    ui->stackedWidget->addWidget(m_menu);
    ui->stackedWidget->setCurrentWidget(m_menu);

    connect(m_menu, &MenuWidget::signalShowBlurMenu, this, &MainWindow::changeStackedWidget);
    connect(m_menu, &MenuWidget::signalCloseApp, this, &MainWindow::close);
    connect(m_menu, &MenuWidget::signalGrayscaleTransformation, this, &MainWindow::grayscaleTransformation);
    connect(m_menu, &MenuWidget::signalCreateHistogram, this, &MainWindow::createHistogram);

    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::close);
    connect(ui->actionLoad_file, &QAction::triggered, this, &MainWindow::loadFile);
    connect(ui->actionSave, &QAction::triggered, this, &MainWindow::saveFile);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::showAbout);

    /* STYLE */
    this->setStyleSheet("MainWindow {background-color: #001a33}");
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_menu;
    delete m_image;

    if (m_help != nullptr)
    {
        delete m_help;
    }
}

void MainWindow::changeStackedWidget()
{
    m_blurMenu = new BlurMenuWidget();

    ui->stackedWidget->addWidget(m_blurMenu);
    ui->stackedWidget->setCurrentWidget(m_blurMenu);

    connect(m_blurMenu, &BlurMenuWidget::signalBack, this, &MainWindow::setMainMenu);
    connect(m_blurMenu, &BlurMenuWidget::signalAverageTransformation, this, &MainWindow::averageTransformation);
    connect(m_blurMenu, &BlurMenuWidget::signalMedianTransformation, this, &MainWindow::medianTransformation);
}

void MainWindow::setMainMenu()
{
    ui->stackedWidget->removeWidget(m_blurMenu);
    ui->stackedWidget->setCurrentWidget(m_menu);

    delete m_blurMenu;
}

void MainWindow::fillImageStruct(const QImage &inputImage)
{
    std::copy(inputImage.bits(), inputImage.bits() + inputImage.byteCount(), std::back_inserter(m_image->data));
    m_image->size.m_width = inputImage.width();
    m_image->size.m_height = inputImage.height();
    m_image->type = ImageType::Rgba;
}

void MainWindow::loadFile()
{
    if (m_image != nullptr)
    {
        m_image->clear();
    }

    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open photo"),
                                                    "",
                                                    tr("Photo (*.jpg);;All Files (*.*)"));

    if (fileName.isEmpty())
    {
        return;
    }

    QImage inputImage(fileName);

    if (inputImage.isNull())
    {
        QMessageBox::information(this, "Error", "File is corrupted");
        return;
    }

    fillImageStruct(inputImage);

    if (inputImage.isGrayscale())
    {
        m_image->type = ImageType::Grayscale;
    }

    createImageForDisplay(*m_image);
}

void MainWindow::saveFile()
{
    if (m_image->data.size() <= 0 || m_image->size.m_height <= 0 || m_image->size.m_width <= 0)
    {
        QMessageBox::warning(this, "Error", "Cannot save file");
    }

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save image"),
                                                    "C://",
                                                    tr("Image (*.jpg)"));

    if (fileName.isEmpty())
    {
        QMessageBox::warning(this, "Error", "Enter file name");
        return;
    }

    bool isSaveSuccess = false;

    if (m_image->type == ImageType::Grayscale)
    {
        const int bytesPerGrayscalePixel = 1;
        QImage output(m_image->data.data(), m_image->size.m_width, m_image->size.m_height,
                      bytesPerGrayscalePixel * m_image->size.m_width, QImage::Format_Grayscale8);

        isSaveSuccess = output.save(fileName, "JPG", 100);
    }
    else
    {
        const int bytesPerARGBPixel = 4;

        QImage output(m_image->data.data(), m_image->size.m_width, m_image->size.m_height,
                      bytesPerARGBPixel * m_image->size.m_width, QImage::Format_ARGB32);

        isSaveSuccess = output.save(fileName, "JPG", 100);
    }

    if (isSaveSuccess)
    {
        QMessageBox::information(this, "Success", "File saved");
    }
    else
    {
        QMessageBox::critical(this, "Failure", "File not saved");
    }
}

void MainWindow::createImageForDisplay(const Image &imageStructure){
    if (imageStructure.type == ImageType::Rgba)
    {
        const int BYTES_PER_ARGB_PIXEL = 4;

        QImage readyImage(imageStructure.data.data(),
                          imageStructure.size.m_width,
                          imageStructure.size.m_height,
                          BYTES_PER_ARGB_PIXEL * imageStructure.size.m_width,
                          QImage::Format_ARGB32);

        displayPhoto(readyImage);
    }
    else
    {
        const int BYTES_PER_GRAYSCALE_PIXEL = 1;

        QImage readyImage(imageStructure.data.data(),
                          imageStructure.size.m_width,
                          imageStructure.size.m_height,
                          BYTES_PER_GRAYSCALE_PIXEL * imageStructure.size.m_width,
                          QImage::Format_Grayscale8);

        displayPhoto(readyImage);
    }
}

void MainWindow::displayPhoto(const QImage &image) const
{
    ui->l_image->setPixmap(QPixmap::fromImage(image));
}

void MainWindow::showAbout()
{
    m_help = new HelpWindow;
    m_help->show();
}

void MainWindow::grayscaleTransformation()
{
    if (m_image->type == ImageType::Grayscale)
    {
        QMessageBox::information(this, "Error", "Image already grayscaled");
        return;
    }

    if (m_image->data.size() <= 0)
    {
        QMessageBox::warning(this, "Error", "First load image");
        return;
    }

    Grayscale grayscale;
    *m_image = grayscale.transform(*m_image);
    createImageForDisplay(*m_image);
}

void MainWindow::createHistogram()
{
    if (m_image->data.size() <= 0)
    {
        QMessageBox::warning(this, "Error", "First load image");
        return;
    }

    Histogram histogram;
    int histogramStatus = histogram.generateHistogram(*m_image);

    if (histogramStatus == 0)
    {
        QMessageBox::information(this, "Histogram", "Saved");
    }
    else
    {
        QMessageBox::warning(this, "Histogram", "Not saved");
    }
}

void MainWindow::averageTransformation(int blurRadius)
{
    if (m_image->data.size() <= 0)
    {
        QMessageBox::warning(this, "Error", "First load image");
        return;
    }

    Average average(blurRadius);
    *m_image = average.transform(*m_image);
    createImageForDisplay(*m_image);
}

void MainWindow::medianTransformation(int blurRadius)
{
    if (m_image->data.size() <= 0)
    {
        QMessageBox::warning(this, "Error", "First load image");
        return;
    }

    Median median(blurRadius);
    *m_image = median.transform(*m_image);
    createImageForDisplay(*m_image);
}
