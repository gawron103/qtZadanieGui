#-------------------------------------------------
#
# Project created by QtCreator 2017-08-21T15:52:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qtZadanieGui
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    menuwidget.cpp \
    blurmenuwidget.cpp \
    average.cpp \
    blur.cpp \
    grayscale.cpp \
    histogram.cpp \
    median.cpp \
    transformation.cpp \
    helpwindow.cpp

HEADERS += \
        mainwindow.h \
    menuwidget.h \
    blurmenuwidget.h \
    average.h \
    blur.h \
    grayscale.h \
    histogram.h \
    image.h \
    median.h \
    transformation.h \
    helpwindow.h

FORMS += \
        mainwindow.ui \
    menuwidget.ui \
    blurmenuwidget.ui \
    helpwindow.ui

RESOURCES += \
    images.qrc

