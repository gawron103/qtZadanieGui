#include "blur.h"

Blur::Blur(int radius) :
    m_BLUR_MASK_RADIUS(radius)
{

}

Blur::~Blur()
{

}

Image Blur::transform(const Image &inputImage)
{
    Image input = (inputImage.type != ImageType::Grayscale)
            ? Grayscale().transform(inputImage)
            : inputImage;

    Image output = input;

    auto indexAt = [&input](int row, int col) { return row * input.size.m_width + col; };
    auto clamp = [&input](int x, int minv, int maxv) { return std::min(std::max(x, minv), maxv); };

    for (int row = 0; row < output.size.m_height; row++)
    { for (int col = 0; col < output.size.m_width; col++)
        {
            std::vector<uint8_t> pixel_values;
            for (int row_offset = -m_BLUR_MASK_RADIUS; row_offset <= m_BLUR_MASK_RADIUS; row_offset++)
            {
                for (int col_offset = -m_BLUR_MASK_RADIUS; col_offset <= m_BLUR_MASK_RADIUS; col_offset++)
                {
                    const int offset_pixel_index = indexAt(clamp(row + row_offset, 0, output.size.m_height - 1), clamp(col + col_offset, 0, output.size.m_width - 1));
                    pixel_values.push_back(input.data[offset_pixel_index]);
                }
            }

            const int center_pixel_index = indexAt(row, col);
            output.data[center_pixel_index] = modifyData(pixel_values);
        }
    }

    return output;
}
