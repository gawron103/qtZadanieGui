#ifndef HELPWINDOW_H
#define HELPWINDOW_H

#include <QDialog>
#include <QGraphicsDropShadowEffect>

namespace Ui {
class HelpWindow;
}

/**
 * @brief The HelpWindow class
 * The task of this class is to show widget with instructions
 */
class HelpWindow : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief HelpWindow -> default constructor
     * @param parent
     */
    explicit HelpWindow(QWidget *parent = 0);
    ~HelpWindow();

private:
    Ui::HelpWindow *ui;

    /**
     * @brief m_shadow -> pointer to QGraphicsDropShadow object
     */
    QGraphicsDropShadowEffect *m_shadow = nullptr;
};

#endif // HELPWINDOW_H
