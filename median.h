#ifndef MEDIAN_H
#define MEDIAN_H

#include <algorithm>

#include "blur.h"

/**
 * @brief The Median class
 * The task of this class is to blur image by use of median
 */
class Median : public Blur
{
public:
    /**
     * @brief Median -> constructor
     * @param givenBlurRadius -> radius for blur
     */
    Median(uint8_t givenBlurRadius);
    ~Median();

    /**
     * @brief processOnMask -> public method that returns the median of vector
     * @param mask -> vector of specific pixel and closest neighbors
     * @return median of vector
     */
    uint8_t modifyData(std::vector<uint8_t> mask);
};

#endif // MEDIAN_H
