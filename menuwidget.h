#ifndef MENUWIDGET_H
#define MENUWIDGET_H

#include <QWidget>
#include <QGraphicsDropShadowEffect>

namespace Ui {
class MenuWidget;
}

/**
 * @brief The MenuWidget class
 * The task of this class is to create and show program main menu
 */
class MenuWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief MenuWidget -> default constructor
     * @param parent
     */
    explicit MenuWidget(QWidget *parent = 0);
    ~MenuWidget();

private:
    Ui::MenuWidget *ui;

    /**
     * @brief m_shadow -> pointer to QGraphicsDropShadow object
     */
    QGraphicsDropShadowEffect *m_shadow = nullptr;

    /**
     * @brief clickedBlurButton -> method that emits signal
     * to MainWindow class
     */
    void clickedBlurButton();
    /**
     * @brief grayscaleTransformation -> method that emits
     * signal to MainWindow class
     */
    void grayscaleTransformation();
    /**
     * @brief createHistogram -> method that emits signal
     * to MainWindow class
     */
    void createHistogram();
    /**
     * @brief closeApp -> method that emits signal to
     * MainWindow class
     */
    void closeApp();

signals:
    /**
     * @brief signalGrayscaleTransformation -> signal for calling
     * grayscale transformation
     */
    void signalGrayscaleTransformation();
    /**
     * @brief signalCreateHistogram -> signal for creating histogram
     */
    void signalCreateHistogram();
    /**
     * @brief signalShowBlurMenu -> signal for switching between
     * menus
     */
    void signalShowBlurMenu();
    /**
     * @brief signalCloseApp -> signal for closing application
     */
    void signalCloseApp();
};

#endif // MENUWIDGET_H
