#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <QImage>

#include <fstream>

#include "image.h"

#include "../googletest/googletest/include/gtest/gtest_prod.h"

/**
 * @brief The Histogram class
 * Class that creates and saved histogram for given image
 */
class Histogram
{
public:
    /**
     * @brief Histogram -> default constructor
     */
    Histogram();
    ~Histogram();
    /**
     * @brief getHistogram -> implemented pure virtual method from class Transformation
     * @param inputImage -> given image
     * @return struct of modified image
     */
    int generateHistogram(const Image& inputImage);

private:
    /**
     * @brief m_programStatus -> holds program status
     */
    int m_programStatus;
    /**
     * @brief createRGBHistogram -> method that creates histogram for every channel
     * @param inputImage -> given image
     * @param histogramFileName -> name of output file
     * @param pixelComponent -> component of pixel
     */
    std::vector<int> createRGBHistogram(const Image &inputImage, const uint8_t pixelComponent);
    /**
     * @brief createGrayscaleHistogram -> method that creates histogram for grayscale image
     * @param inputImage -> given image
     */
    std::vector<int> createGrayscaleHistogram(const Image &inputImage);
    /**
     * @brief drawHistogram -> method that creates 2D vector and fills it with
     * data from channel
     * @param histogramData -> channel data vector
     * @param fileName -> name for output file
     */
    int drawHistogram(const std::vector<int> &histogramData, const std::string fileName);
    /**
     * @brief saveHistogramImage -> method that saves histogram as image file
     * @param fileName -> output name for file
     * @param data -> vector of image data
     * @param width -> width of histogram image
     * @param height -> height of histogram image
     */
    int saveHistogramImage(const std::string &fileName, const std::vector<uint8_t> data, const int width, const int height);

    FRIEND_TEST(HistogramTest, ConstructorTest);
    FRIEND_TEST(HistogramTest, createPositiveRGBHistogramTest);
    FRIEND_TEST(HistogramTest, createPositiveGrayscaleHistogramTest);
    FRIEND_TEST(HistogramTest, drawHistogramTest);
};

#endif // HISTOGRAM_H
